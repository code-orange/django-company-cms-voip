from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)


def home(request):
    template = loader.get_template("django_company_cms_voip/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Voice over IP")
    template_opts["content_title_sub"] = _("What is that?")

    template_opts["product_voip_home_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_voip_home_01_header"
    ).content

    template_opts["product_voip_home_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_voip_home_01_content"
    ).content

    template_opts["product_voip_home_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_voip_home_02_header"
    ).content

    template_opts["product_voip_home_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_voip_home_02_content"
    ).content

    template_opts["product_voip_home_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_voip_home_03_header"
    ).content

    template_opts["product_voip_home_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_voip_home_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def cloud_pbx(request):
    template = loader.get_template("django_company_cms_voip/cloud_pbx.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Voice over IP")
    template_opts["content_title_sub"] = _("CloudPBX")

    template_opts["product_cloud_pbx_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_cloud_pbx_01_header"
    ).content

    template_opts["product_cloud_pbx_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_cloud_pbx_01_content"
    ).content

    template_opts["product_overview_title"] = _("Product Features")

    product_list = list()

    cloud_pbx_product_filter_1 = "product_cloud_pbx_item_"
    cloud_pbx_product_filter_2 = "_name"

    cloud_pbx_products = CompanyCmsGeneral.objects.filter(
        name__startswith=cloud_pbx_product_filter_1,
        name__endswith=cloud_pbx_product_filter_2,
    )

    for cloud_pbx_product in cloud_pbx_products:
        key = cloud_pbx_product.name[
            : len(cloud_pbx_product.name) - len(cloud_pbx_product_filter_2)
        ]

        product_list.append(
            {
                "name": CompanyCmsGeneral.objects.get(name=key + "_name").content,
                "subtitle": CompanyCmsGeneral.objects.get(
                    name=key + "_subtitle"
                ).content,
                "icon": CompanyCmsGeneral.objects.get(name=key + "_icon").content,
            }
        )

    template_opts["product_list"] = product_list

    return HttpResponse(template.render(template_opts, request))


def national_code_details(request, country_iso: str, national_code: int):
    template_opts = dict()

    template_opts["content_title_main"] = _("National Phone Code")
    template_opts["content_title_sub"] = _("Overview")

    return HttpResponse(str(national_code))
