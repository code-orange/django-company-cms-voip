from datetime import datetime

from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from django_mdat_phone.django_mdat_phone.models import *
from . import views


class VoipStaticViewSitemap(Sitemap):
    def items(self):
        return ["voip", "voip_cloud_pbx"]

    def location(self, item):
        return reverse(item)


class VoipNationalCodeDetailsSitemap(Sitemap):
    def items(self):
        return (
            MdatPhoneNationalCode.objects.all()
            .values("national_code", "city__country__iso")
            .distinct()
        )

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(
            views.national_code_details,
            args=[obj["city__country__iso"].lower(), obj["national_code"]],
        )
