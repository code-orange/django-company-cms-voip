from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="voip"),
    path("cloud-pbx", views.cloud_pbx, name="voip_cloud_pbx"),
    path(
        "nc/<str:country_iso>/<int:national_code>",
        views.national_code_details,
        name="voip_national_code_details",
    ),
]
